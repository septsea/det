# Source code files of this book

In this folder are most of the source code files (but not all) of this book. The book was typeset in LaTeX. Additionally, this (main) file is needed to build the whole book:
- [`libro.tex`](../libro.tex)
