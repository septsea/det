# The Esperanto expressions used in this book

Well, I used some Esperanto expressions in this book, even though this book is (currently) only in the Chinese language, for some reason that I have forgotten. I am not an evil person (am I?), so I will explain the meaning of those Esperanto expressions, for persons who do not necessarily know Esperanto, in the order of appearance (chronologically).

Esperanto (literally "one who hopes"), created by Zamenhof, Ludwik Łazarz in 1887, is probably the most widely spoken constructed international auxiliary language of the world.

---

> Determinantulo

(literally) a person who is associated with determinants

---

> Ĉi tiu libro estas disponebla laŭ la [Nul-Kondiĉa Permesilo de BSD](https://opensource.org/license/0bsd) (mallongigo de angle _Berkeley Software Distribution_). Jen [**neoficiala** traduko de la permesilo en Esperanton](https://github.com/septsea/permesiloj/blob/cxefa/0bsd.md). Ĝi estas provizita por helpi pli multajn homojn kompreni la permesilon. Ĝi ne estis publikigita de la aŭtoro de la permesilo, kaj **ne** laŭleĝe deklaras la distribukondiĉojn por programaro, kiu uzas la permesilon—nur la originala angla teksto de la permesilo faras tion.

This book is available under the [Zero-Clause BSD License](https://opensource.org/license/0bsd) (in which BSD stands for _Berkeley Software Distribution_). Here is [an **unofficial** translation of the licence into Esperanto](https://github.com/septsea/permesiloj/blob/cxefa/0bsd.md). It is provided to help more persons understand the licence. It was not published by the author of the licence, and **does not** legally state the distribution terms for software that uses the licence—only the original English text of the licence does that.

---

> Aŭtorrajto © 2025 Determinantulo
>
> Permeso uzi, kopii, modifi kaj/aŭ distribui ĉi tiun programaron por iu ajn celo kun aŭ sen pago estas ĉi tie donita.
>
> **La programaro estas provizita "tiel, kiel estas", kaj la aŭtoro ne donas iujn ajn garantiojn rilate al ĉi tiu programaro, inkluzive de ĉiuj implicitaj garantioj pri merkatkapableco kaj taŭgeco. En neniu okazo la aŭtoro estu respondeca pri iuj ajn specialaj, rektaj, nerektaj, aŭ konsekvencaj damaĝoj aŭ iaj ajn damaĝoj rezultantaj el perdo de uzado, datumoj aŭ profitoj, ĉu en ago de kontrakto, neglektado aŭ alia delikta ago, sekvantaj el aŭ rilataj al la uzado aŭ rendimento de ĉi tiu programaro.**

Copyright © 2025 Determinantulo

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

**The software is provided "as is" and the author disclaims all warranties with regard to this software including all implied warranties of merchantability and fitness. In no event shall the author be liable for any special, direct, indirect, or consequential damages or any damages whatsoever resulting from loss of use, data or profits, whether in an action of contract, negligence or other tortious action, arising out of or in connection with the use or performance of this software.**

---

> Ĉi tiu libro estas dediĉita al ĉiuj homoj.

This book is dedicated to all persons.

---

> La lasta leciono

The last lesson

<!-- ---

> determinanto

a determinant -->

<!-- ---

> sumo

a sum -->

<!-- ---

> produto

a product _(arithmetic)_ -->

<!-- ---

> simplektika matrico

a symplectic matrix -->

---

> _Enkonduko al determinantoj_

_An introduction to determinants_
