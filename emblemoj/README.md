# The emblem made for this book

Mi dankas [Petrica](https://github.com/PetricaT) pro fari VTuber-stilan emblemon por ĉi tiu libro:  
I thank [Petrica](https://github.com/PetricaT) for making a VTuber-fashioned emblem for this book:  
我感谢 [Petrica](https://github.com/PetricaT) 为本书作了一个 VTuber 风格的标识:

https://github.com/PetricaT/ProgrammingVTuberLogos-Addon/tree/main/Determinant

---

Postscript on 2024-08-31:

Not only has Petrica made this emblem for free, Petrica has also granted me permission to use this emblem however I would like to (see [https://github.com/PetricaT/ProgrammingVTuberLogos-Addon/issues/2#issuecomment-2104372637](https://github.com/PetricaT/ProgrammingVTuberLogos-Addon/issues/2#issuecomment-2104372637)). For this reason, I license this emblem under [0BSD](https://opensource.org/license/0bsd), which is the same licence as the one that this book uses. I thank Petrica very much. It would be great if I could thank Petrica better. I hope that Petrica is doing well and will do well.
