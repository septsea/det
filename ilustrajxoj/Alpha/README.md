# Illustration by Alpha

Mi dankas Alpha pro desegni ĉi tiun ilustraĵon por ĉi tiu libro. Mi dankas [王一尾](https://space.bilibili.com/12952643/) pro esti bona makleristo por ĉi tiu ilustraĵo.  
I thank Alpha for drawing this illustration for this book. I thank [王一尾](https://space.bilibili.com/12952643/) for being a good broker for this illustration.  
我感谢 Alpha 为本书作此插画. 我感谢[王一尾](https://space.bilibili.com/12952643/)作中介.

**Oni rajtas kopii kaj distribui ĉi tiun ilustraĵon en iu ajn medio aŭ formato por iu ajn celo, eĉ komerce, se oni donas taŭgan krediton kaj ne distribuas derivaĵojn de ĉi tiu ilustraĵo.**  
**One is free to copy and distribute this illustration in any medium or format for any purpose, even commercially, provided that one gives appropriate credit and does not distribute derivatives of this illustration.**  
**可以自由复制和分发此插画, 以任何媒介或格式用于任何目的, 甚至商业目的, 但必须注明出处, 并且不分发此插画的衍生品.**

---

<p align="center">
    <img src="../../ilustrajxoj/Alpha/illustration-20240828.jpg" width="62.5%" height="62.5%" alt="ilustraĵo" />
</p>

---

Postscript:

In accordance with the request of Alpha, the contact details of Alpha are not revealed.
