# The image files used in this book

Metu bildodosierojn en la libro en ĉi tiun dosierujon.  
Put image files in the book into this folder.  
置书里的图片文件于此文件夹.

---

Postscript on 2024-08-31:

Well, actually, there is currently no image file used in this book. It is not likely that any image file will ever be used in this book, for various reasons, including but not limited to copyright.

However, it was the case that there were image files. I used four countdown artworks
- https://twitter.com/burumakun/status/1570336664131891201
- https://twitter.com/burumakun/status/1569974279856439296
- https://twitter.com/burumakun/status/1569611912714899456
- https://twitter.com/burumakun/status/1569258882148757504

drawn by Hiroyuki (ヒロユキ), each featuring one of the four girls in his comic (manga) _Girlfriend, girlfriend_ ("カノジョも彼女", literally _She is also a girlfriend_). I wrote one chapter and three appendices (or rather, annexes) in my book, so I believed that I used the artworks really perfectly, one image being on the opening page of each chapter (or appendix/annex), if there were no such thing as copyright.

It might be noted that I have mentioned _copyright_ more than one time. Yeah, the biggest problem is exactly copyright: I cannot use his artworks in my book without permission.
