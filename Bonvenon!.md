# Bonvenon!

Bonvenon! Ĉi tio estas _Enkonduko al determinantoj_.  
Welcome! This is _An introduction to determinants_.  
欢迎! 这是《行列式入门》.

Malfermu la dosieron [`README.md`](./README.md) por legi tion, pri kio ĉi tiu libro estas.  
Open the file [`README.md`](./README.md) to read about what this book is.  
打开文件 [`README.md`](./README.md) 以了解更多.

Mi esperas, ke mia libro estas utila.  
I hope that my book is useful.  
我希望我的书是有用的.
